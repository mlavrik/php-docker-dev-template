# PHP Docker dev environment template

## Commands

`make up` - run all containers via docker compose

`make build` - force php image to rebild and then run all containers

`make down` - stop all containers

`make restart` - restart by make down and then up again

`make shell` - enter php container shell


## Services

### Nginx

Main config: `docker/nginx/nginx.conf`

Virtual host config: `docker/nginx/conf.d/vhost.conf`

Public root directory `www`

### Mysql

User: `root`

Password: `root`

Port: `3306`

### [phpMyAdmin](http://localhost:8080/)

Port: `8080`

### PHP

Config: `docker/php/php.ini`

### Xdebug

Config: `docker/php/xdebug.ini`

Create Server in IDE config with name `docker` and map project files to container `/var` folder
