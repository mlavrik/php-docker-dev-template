COMPOSE_FILE = docker/docker-compose.yml

up:
	docker compose -f ${COMPOSE_FILE} up -d

build:
	docker compose -f ${COMPOSE_FILE} up -d --build

down:
	docker compose -f ${COMPOSE_FILE} down

restart: down up

shell:
	docker exec -it docker-app-1 bash
